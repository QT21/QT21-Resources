This git contains all resources produced by the QT21 research project: software, tools and data.

The project QT21 (Quality Translation) has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No. 645452

QT21 is about improving Machine Translation for morphologcally rich languages.