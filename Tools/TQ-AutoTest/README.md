# QT21 #

## Project ##
This project is a tool for checking translation quality automatically, written in scala with Play framework, current deployed on http://lnv-3233.sb.dfki.de:9000/ . The database that contains sentence templates and regular expression rules is not included.

### Debug ###
run
```
./activator run
```
to start the app.

### Deploying on server ###

Current server is wizard@lnv-3233.sb.dfki.de, on which all files should go to /local/www/ folder. Since the server space is limited, please clone this repo on your machine and build, and copy the generated distribution to server.

```
cd project_folder
./activator dist
scp target/universal/qt21-1.0-SNAPSHOT.zip wizard@lnv-3233.sb.dfki.de:/local/www/renlong/
```

and then log into server, unzip the zip file and run ./bin/qt21 to start the server. You will need your ssh key added to the server, please talk to Sven or directly to ISG.
```
ssh wizard@lnv-3233.sb.dfki.de
cd /local/www/renlong
unzip qt21-1.0-SNAPSHOT.zip
cd qt21-1.0-SNAPSHOT
nohup ./bin/qt21 &
```

##### Note: #####
* you might want to remove the old qt21-1.0-SNAPSHOT.zip and qt21-1.0-SNAPSHOT folder before new deployment.
* run with nohup to ensure the app keeps running after you log out.
* the server might stop sometimes for unknown reason, to restart the app just run ```nohup ./bin/qt21 &``` again. If it tells you the program is already running, just delete the file RUNNING_PID as it tells you.

